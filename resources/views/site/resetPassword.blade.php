@extends('User.layout.master')
@section('content')
    {{dd ($token)}}
    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>تغير كلمة المرور</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form forget-panel">

                    {{--<form action="{{ route('password.update') }}" method="post" enctype="multipart/form-data">--}}
                    <form action="{{ route('UPDATE_PASSWORD') }}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="up_form-item">
                            <input type="email" name="email" placeholder="example@example.com" data-placement="top"
                                   title="اكتب البريد الاليكتروني " data-toggle="tooltip">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="password" name="password" placeholder="------" data-placement="top"
                                   title="اكتب كلمة المرور الجديدة" data-toggle="tooltip">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="password" name="password_confirmation" placeholder="------"
                                   data-placement="top" data-toggle="tooltip"
                                   title="أعد كتابة كلمة المرور الجديدة">
                        </div>
                        <!-- /.up_form-item -->

                        <!-- /.up_form-item -->
                        <div class="up_form-item up-confirm">
                            <input type="submit" value="حفظ">
                        </div>
                        <!-- /.up_form-item -->
                    </form>

                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>


@stop