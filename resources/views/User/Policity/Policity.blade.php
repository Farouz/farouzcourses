@extends('User.layout.master')
@section('Title')

    سياسه الخصوصيه
    @stop
@section('content')

    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>تعرف علي سياسة الخصوصية لدي المدرب</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form">
                    <div class="privacy-box text-right">
                        @foreach($settings as $setting)
                        <p>{{$setting->policity}}</p>
@endforeach

                    </div>
                    <!-- /.privacy-box -->
                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>


    @stop