@extends('User.layout.master')
@section('Title')
    Home
@stop

@section('content')
    <div class="search-box">
        <div class="container">
            <div class="search-inner">
                <h1 class="text-center">تستطيع من خلال موقعنا البحث عن كل ما تريد من كورسات </h1>
                <form action="{{route('SEARCH_FOR_COURSES')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-item col-xs-12">
                        <div class="input-container col-md-10 col-xs-12 input-lft pull-right">
                            <input type="text" name="course_search" placeholder="ابحث عن جميع الكورسات من هنا">
                        </div>
                        <!-- /.input-container -->
                        <div class="btn-container col-md-1 btn-right">
                            <a class="show-advanced">
                                بحث متقدم
                            </a>
                        </div>
                        <!-- end btn-container -->
                        <div class="btn-container col-md-1">
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                        <!-- end btn-container -->
                    </div>
                    <!-- /.form-item -->
                    <div class="form-advanced col-xs-12 adv-left">
                        <div class="advanced-item col-md-3 col-xs-12 pull-right">
                            <h2>ابحث بأسم المدرس</h2>
                            {{--<input type="text" name="teacher_name" placeholder="ابحث بإسم المدرس">--}}
                            <select name="teacher_id" class="form-control" style="
    background: transparent;
    border: 0;
    outline: none;
    color: #fff;
    border-bottom: 1px solid #ffffffcf;
    height: 40px;
">
                                <option>اسم المدرس</option>

                                @foreach($teachers as $teacher)
                                    <option value="{{$teacher->id}}" style="    box-sizing: border-box;
    color: #0D8F48;
    outline: none;
    border: 0;">{{$teacher->fullName}}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- /.advanced-item -->

                        <!-- /.advanced-item -->
                        <div class="advanced-item col-md-3 col-xs-12 pull-right">
                            <h2>ابحث في المجالات </h2>
                            <select name="cat_id" class="form-control" style="
    background: transparent;
    border: 0;
    outline: none;
    color: #fff;
    border-bottom: 1px solid #ffffffcf;
    height: 40px;
">
                                <option>اسم القسم</option>
                                @foreach($cats as $cat)
                                    <option value="{{$cat->id}}" style="    box-sizing: border-box;
    color: #0D8F48;
    outline: none;
    border: 0;">{{$cat->title_ar}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!-- /.form-advanced -->
                </form>
            </div>
            <!-- /.search-inner -->
        </div>
        <!-- /.container -->
    </div>

    <div class="courses">
        <div class="container">
            <div class="courses-head text-center">
                <h1>أحدث الكورسات</h1>
            </div>
            <!-- /.testominal-head -->
            <div class="row block-container">
                @foreach($courses as $course)
                    <div class="block col-md-4 col-sm-6">
                        <figure>
                            <div><img src="{{asset('User/images/'.$course->course_image)}}" alt="img05"
                                      class="img-responsive"></div>
                            <figcaption class="text-right">
                                <h1>
                                    <label>اسم الكورس</label>
                                    <span>{{$course->course_title}}</span>
                                </h1>
                                <h1>
                                    <label>اسم المدرس</label>
                                    <span>{{App\User::where('id',$course->teacher_id)->first()->fullName}}</span>

                                </h1>
                                <h1>
                                    <label>عدد الطلبة المشتركة</label>
                                    <span><?php
                                        $students = \Illuminate\Support\Facades\DB::table('courses_users')->select('user_id')->where('course_id', $course->id)->count();
                                        echo $students;
                                        ?></span>

                                </h1>
                                <h1>
                                    <label>تاريخ بداية الكورس</label>
                                    <span> {{$course->course_date}}</span>

                                </h1>
                                <h1>
                                    <label>تقييم الكورس</label>
                                    <span><?php
                                        $items = \App\Rates::where('course_id', $course->id)->count('rate');
                                        $all = \App\Rates::where('course_id', $course->id)->sum('rate');
                                        if (!$items == 0) {
                                            $sum = $all / $items;
                                        } else {
                                            $sum = 0;
                                        }
                                        for ($i = 0; $i < $sum; $i++) {
                                            echo ' <i class="fa fa-star active" style="color: yellow"></i>';

                                        }
                                        echo '<i class="fa fa-star-half-full" style="color: yellow"></i>'
                                        ?> </span>

                                </h1>
                                <a href="{{route('GET_THIS_COURSE',$course->id)}}">
                                    <i class="fa fa-eye"></i> مشاهدة الكورس
                                </a>
                            </figcaption>
                        </figure>
                    </div>
                @endforeach

            </div>
            <!-- /.row -->

            <div class="all-courses text-center">
                <a href="{{route('GET_ALL_COURSES')}}">عرض جميع الكورسات</a>
            </div>
            <!-- /.all-courses -->

        </div>
        <!-- /.conainer -->
    </div>
    <!-- /.courses -->

    <div class="testominal">
        <div class="overlay"></div>
        <div class="container">
            <div class="testo-head text-center">
                <h1>قالوا عنا</h1>
            </div>
            <!-- /.testominal-head -->
            <div class="testo-slider text-center">
                @foreach($customers as $customer)
                    <div class="testo-item col-xs-12">

                        <p>{{$customer->desc}}</p>
                        <div class="testo-img">
                            <img src="{{asset('public/User/images/'.$customer->image)}}" alt="" class="img-responsive">
                        </div>
                        <!-- /.testo-img -->
                        <h1>{{$customer->name}}</h1>
                        <!-- /.testo-img -->
                    </div>
                @endforeach
            </div>
            <!-- /. testo-slider -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.testominal -->
@stop
@section('footer')
    <div class="footer-sub col-md-2 col-xs-12 text-center pull-right">
        <ul>
            <li>
                <a href="{{route('GET_ABOUT')}}">من نحن</a>
            </li>

            <li>
                <a href="{{route('GET_CONTACT')}}">إتصل بنا</a>
            </li>
        </ul>
    </div>
@stop