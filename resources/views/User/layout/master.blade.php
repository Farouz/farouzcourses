<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 ie-all" lang="en-US" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 9]>
<html class="ie9 ie-all" lang="en-US" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 10]>
<html class="ie10 ie-all" lang="en-US" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if !IE]><!-->
<!--<![endif]-->
<html>

<head>
    <title> @yield('Title')</title>
    @yield('AboveScripts')
    <meta name="author" content="Hossam Farouz">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <meta charset="utf-8">

    <!-- Css Files -->
    <link href="{{asset('User/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/font-awesome.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.theme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/selectric.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/reset.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/images/favicon.png')}}" rel="icon" type="text/css">
    @yield('UserCss')
</head>

<body>

<!-- start the loading screen -->
<div class="wrap">
    <div class="loading">
        <div class="bounceball"></div>
        <div class="text">NOW LOADING</div>
    </div>
</div>

<!-- end the loading screen -->

<div class="wrapper st-container" id="st-container">
    <!-- content push wrapper -->
    <div class="st-pusher">

        <nav class="st-menu st-effect-8" id="menu-8">
            @foreach($settings as $setting)
                <h2 class="icon icon-stack">
                    <img src="{{asset('public/User/images/'.$setting->site_image)}}" class="img-responsive">

                </h2>
            @endforeach
            <ul>
                <li><a class="icon icon-data" href="{{route('home')}}"><i class="fa fa-user"></i> الرئيسية</a></li>
                <li><a id="sd" class="icon icon-location" href="{{route('GET_ABOUT')}}"><i class="fa fa-group"></i>من
                        نحن</a></li>
                @if(Auth::check())
                    @if(Auth::user()->is_teacher == 1)
                        <li><a class="icon icon-study" href="{{route('GET_ADD_COURSE')}}"><i class="fa fa-plus"></i>
                                اضافة دورة</a></li>
                    @endif
                    <li><a class="icon icon-data" href="{{route('GET_ALL_COURSES')}}"><i class="fa fa-database"></i>جميع
                            الدورات</a></li>
                    <li><a class="icon icon-location" href="{{route('GET_ALL_COURSES_CATEGORIES')}}"><i
                                    class="fa fa-rocket"></i>جميع الاقسام</a></li>
                    <li><a class="icon icon-wallet" href="{{route('GET_USER_PROFILE',Auth::user()->id)}}"><i
                                    class="fa fa-user"></i>الحساب الشخصي</a></li>
                @endif
                <li><a class="icon icon-photo" href="{{route('GET_CONTACT')}}"><i class="fa fa-phone"></i>اتصل بنا</a>
                </li>
                <li><a class="icon icon-photo" href="{{route('GET_POLICITY')}}"><i class="fa fa-lock"></i>سياسة الخصوصية</a>
                </li>
                @if(Auth::check())
                    <li><a class="icon icon-data" href="{{route('LOG_OUT')}}"><i class="fa fa-sign-out"></i>تسجيل الخروج</a>
                    </li>
                @endif
            </ul>
        </nav>
        <div class="st-content">
            <div class="dividers">
                <span class="t1"></span>
                <span class="t2"></span>
                <span class="t3"></span>
                <span class="t4"></span>
                <span class="t5"></span>
                <span class="t1"></span>
                <span class="t2"></span>
                <span class="t3"></span>
                <span class="t4"></span>
                <span class="t5"></span>
            </div>
            <!-- /.dividers -->

            <div id="st-trigger-effects" class="column">

                <button data-effect="st-effect-8" class="st_show">
                    <i class="fa fa-bars"></i>
                </button>
            </div>
            <header>

                <div class="error-detect">
                    <div class="container">
                        <div class="error text-center">
                            <h1 class="danger-l">اي كلام اي كلام اي كلام يا حسني اي كلام يا حسني اي كلام</h1>
                            <h1 class="message-l">اي كلام اي كلام اي كلام يا حسني اي كلام يا حسني اي كلام</h1>
                            <h1 class="success-l">اي كلام اي كلام اي كلام يا حسني اي كلام يا حسني اي كلام</h1>
                        </div>
                        <!-- /.error-danger -->
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /.error-detect -->
                <div class="login-area">
                    <div class="container">
                        <div class="login-form col-md-6 col-xs-12 text-right pull-right">
                            <h1>تسجيل الدخول</h1>
                            <form action="{{route('POST_LOGIN')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="login-item">
                                    <input name="userName" type="text" placeholder="إسم المستخدم">
                                </div>
                                <!-- /.login-item -->
                                <div class="login-item">
                                    <input type="password" name="password" placeholder="كلمة السر">
                                </div>
                                <!-- /.login-item -->
                                <div class="login-item">
                                    <label class="pull-right">
                                        <input type="checkbox">
                                        <span>تذكر كلمة السر دائماً</span>
                                    </label>
                                    <label class="pull-left">
                                        <a href="#" class="forget">هل نسيت كلمة المرور ؟</a>
                                    </label>
                                </div>
                                <!-- /.login-item -->
                                <div class="login-item">
                                    <input type="submit" value="دخول">
                                </div>

                            </form><!-- /.login-item -->
                        </div>
                        <!-- /.login-form -->

                        <div class="signup-form col-md-6 col-xs-12 text-right">
                            <h1>تسجيل عضوية جديدة</h1>
                            <p>اذا كنت مستخدم جديد لموقعنا فيمكنك ان تتصفح معظم الكورسات الموجودة الان امامك ولكن لن
                                تستطيع الحصول علي معلومات الكورس او الاشتراك به الا اذا كنت تمتلك حساب لدينا لذلك تستطيع
                                تسجيل حساب جديد من هنا </p>
                            <a href="{{route('GET_SIGNUP')}}">
                                <i class="fa fa-user-plus"></i> تسجيل عضوية
                            </a>
                        </div>
                        <!-- /.signup-form -->

                        <!-- =========================================================================================================================================== -->
                        <form method="post" action="{{ url('/password/email') }}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="panel-pop modal" id="forget">
                                <div class="lost-inner">
                                    <h1>هل نسيت كلمة المرور ؟</h1>
                                    <div class="lost-item">
                                        <input type="text" name="email" placeholder="الايميل المستخدم في تسجيل الدخول">
                                    </div>
                                    <!-- /.lost-item -->
                                    <div class="text-center">
                                        <input type="submit" value="إعادة ضبط">
                                    </div>
                                    <!-- /.lost-item -->
                                </div>
                                <!-- /.lost-inner -->
                            </div>
                        </form>

                        <!-- /.modal -->

                        <!-- =========================================================================================================================================== -->

                    </div>
                    <!-- /.container -->
                </div>
                <!-- /.login-area -->

                <div class="header-nav">
                    <div class="container">
                        <div class="nav-right col-md-8 col-xs-12 pull-right">
                            <div class="logo">
                                <a href="#" title="العلوم العصرية للتدريب">
                                    <img src="{{asset('User/images/logo.png')}}" alt="site-logo" width="110"
                                         height="70">
                                </a>
                            </div>


                        </div>
                        <!-- /.nav-logo -->
                        <div class="nav-left col-md-4 col-xs-12 pull-left">

                            <div class="user-controls">
                                <ul>
                                    @if(!Auth::check())
                                        <li>
                                            <a href="#" class="show-login">
                                                <i class="fa fa-user"></i> منطقة تسجيل الدخول
                                            </a>
                                        </li>

                                    @endif


                                </ul>
                            </div>
                            <!-- /.user-controls -->
                        </div>

                        <!-- /.nav-user -->
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /.header-nav -->
            </header>
            <!-- /header -->
            @include('User.layout.messages')
            @yield('content')

            <footer>
                <div class="container">
                @yield('footer')
                <!-- end footer-sub -->
                    <div class="copyrights col-md-8 col-xs-12 text-center pull-right">
                        <p>حميع الحقوق محفوظة لدي شركه IEASOFT</p>
                    </div>
                    <!-- /.copyrights -->
                    <div class="footer-links col-md-2 col-xs-12 pull-left text-center">
                        <ul>
                            @foreach($settings as $setting)
                                <li>
                                    <a href="{{$setting->facebook}}" data-toggle="tooltip" data-placement="top"
                                       title="facebook">
                                        <i class="fa fa-facebook-square"></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{$setting->twitter}}" data-toggle="tooltip" data-placement="top"
                                       title="twitter">
                                        <i class="fa fa-twitter-square"></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{$setting->linkden}}" data-toggle="tooltip" data-placement="top"
                                       title="linkedin">
                                        <i class="fa fa-linkedin-square"></i>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.footer-links -->
                </div>
                <!-- /.container -->
            </footer>

        </div>
        <!-- /st-content -->
    </div>
</div>
<!-- /.wrapper -->

<div class="toTop col-xs-12 text-center">
    <i class="fa fa-angle-up"></i>
</div>
<!-- /.toTop -->


<!-- Javascript Files -->
<script src="{{asset('User/js/jquery-2.2.2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/html5shiv.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/jquery-smoothscroll.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/modernizr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/wow.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/placeholdem.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/toucheffects.js')}}"></script>
<script src="{{asset('User/js/jquery.selectric.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/classie.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/jquery.nicescroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/script.js')}}" type="text/javascript"></script>
@yield('UserScripts')
</body>

</html>