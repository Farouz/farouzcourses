@extends('User.layout.master')
@section('Title')
    {{$course->course_title}}
@stop
@section('content')
    <div class="intro-container">
        <div class="intro-head text-center">
            <div class="container">
                <h1>{{$course->course_title}}</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.intro-head -->
        <div class="intro-box">
            <div class="container">
                <div class="intro-name text-right">
                    <div class="name-head col-md-7 col-xs-12 pull-right">
                        <h1>{{$course->course_title}}</h1>
                    </div>
                    <div class="extras col-md-5 col-xs-12">
                        @if($course->course_salary==null)
                            <span>FREE</span>
                        @else
                            <span>{{$course->course_salary}}</span>
                        @endif
                        <div class="intro-rating">
                            <ul>
                                <?php
                                $items = \App\Rates::where('course_id', $course->id)->count('rate');
                                $all = \App\Rates::where('course_id', $course->id)->sum('rate');
                                if (!$items == 0) {
                                    $sum = $all / $items;
                                } else {
                                    $sum = 0;
                                }
                                for ($i = 0; $i < $sum; $i++) {
                                    echo ' <li>
                                        <i class="fa fa-star" style="color: yellow" ></i>

                                </li>';

                                }
                                echo '<i class="fa fa-star-half-full" style="color: yellow"></i>'
                                ?>
                            </ul>
                        </div>
                        <!-- end intro-rating -->
                    </div>
                </div>
                <!-- /.intro-name -->
                <div class="intro-video col-xs-12 text-center">
                    <?php
                    if (!$course->course_video_url == null) {
                        $video = Embed::make($course->course_video_url)->parseUrl();
                        if ($video) {
                            $video->setAttribute(['width' => 700]);
                            echo $video->getHtml();
                        }
                    } else {
                        $video = Embed::make($course->course_video)->parseUrl();
                        if ($video) {
                            $video->setAttribute(['width' => 300]);
                            echo $video->getHtml();
                        }
                    }

                    ?>
                </div>
                <!-- /.intro-video -->
                <div class="intro-date col-xs-12 text-right">
                    <h1>
                        <i class="fa fa-calendar"></i>
                        Course Will Start At : {{$course->course_date}}
                    </h1>

                    @if(! Auth::user()->Courses()->where('course_id',$course->id)->exists())
                        @if($course->course_payment_method==1)
                            <a href="{{route('GET_START_COURSE',$course->id)}}">
                                <i class="fa fa-paper-plane"></i> إشترك في الدورة
                            </a>

                        @elseif($course->course_payment_method==2)
                            <a href="{{route('GET_BUY_COURSE',$course->id)}}">
                                <i class="fa fa-paper-plane"></i>الدفع بواسطه payPal
                            </a>


                            {{--TO REAL TRANSACTIONS--}}
                            {{--<form action="https://www.paypal.com/cgi-bin/webscr" method="post">--}}
                            {{--{{csrf_field()}}--}}

                            {{--<input type="hidden" name="cmd" value="_xclick">--}}
                            {{--<input type="hidden" name="business" value="hossamfarouz15@gmail.com">--}}
                            {{--<input type="hidden" name="item_name" value="{{$course->course_title}}">--}}
                            {{--<input type="hidden" name="item_number" value="1">--}}
                            {{--<input type="hidden" name="amount" value="{{$course->course_salary}}">--}}
                            {{--<input type="hidden" name="no_shipping" value="0">--}}
                            {{--<input type="hidden" name="no_note" value="1">--}}
                            {{--<input type="hidden" name="return" value="http://localhost/laCourseDeFarouz">--}}
                            {{--<input type="hidden" name="currency_code" value="USD">--}}
                            {{--<input type="hidden" name="lc" value="AU">--}}
                            {{--<input type="hidden" name="bn" value="PP-BuyNowBF">--}}
                            {{--<input type="submit" value="Pay with PayPal!" class="btn btn-facebook" style="float: left; background-color:forestgreen ;color: white;width: 200px;height: 40px" placeholder="&#61447;">--}}
                            {{--<button type="submit" class="btn btn-success" style="float: left"><i class="fa fa-paper-plane-o"></i> Pay With PayPal</button>--}}
                            {{--<img alt="" border="0" src="https://www.paypal.com/en_AU/i/scr/pixel.gif" width="1"--}}
                            {{--height="1">--}}
                            {{--</form>--}}

                        @endif
                        @if(isset($method))
                            @if($method->state == 'aproved')
                                <a href="{{route('GET_START_COURSE',$course->id)}}">
                                    <i class="fa fa-paper-plane"></i> عرض الدوره
                                </a>
                            @endif
                        @endif
                    @else

                        <a href="{{route('GET_START_COURSE',$course->id)}}">
                            <i class="fa fa-paper-plane"></i> عرض الدوره
                        </a>
                    @endif

                </div>
                <!-- /.intro-date -->
                <div class="intro-details text-right">
                    <p>
                        {{$course->course_description}}
                    </p>
                </div>
                <!-- /.intro-details -->

                <div class="intro-extra col-xs-12">
                    <div class="intro-instructor col-md-6 col-xs-12 text-right pull-left">
                        <div class="intro_instructor-inner">
                            <h1>عن المدرس</h1>
                            <div class="avatar text-center">
                                <div class="av-inner">
                                    <img src="{{asset('User/images/s.png')}}" alt="" width="80" height="80">
                                </div>
                                <!-- /.av-inner -->
                            </div>
                            <!-- /.avatar -->
                            <div class="instructor-data">

                                <a href="#" class="know-teacher" data-toggle="tooltip" data-placement="top"
                                   title="اضغط لمعرفة هوية المحاضر">{{\App\User::where('id',$course->teacher_id)->first()->fullName}}</a>
                                <p>{{$teacher_cv}}</p>
                            </div>
                            <!-- /.instructor-data -->
                        </div>
                        <!-- /.intro_instructor-inner -->
                    </div>
                    <!-- /.intro-instructor -->
                    <div class="intro-lec col-md-6 col-xs-12 text-right pull-right">
                        <div class="intro_lec-inner">
                            <h1>ماذا يحتوي هذا الكورس</h1>

                            <ol>
                                @foreach($lectures as $lecture)
                                    <li>
                                        <i class="fa fa-play-circle"></i> {{$lecture->lecture_title}}
                                    </li>
                                @endforeach
                            </ol>

                        </div>
                        <!-- /.intro_lec-inner -->
                    </div>
                    <!-- /.intro-lec -->
                </div>
                <!-- /.intro-extra -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /.intro-box -->
    </div>


@stop

@section('UserScripts')
    <script type="text/javascript">
        var myPlayer = videojs("example_video_1");

        $('#show-l10').click(function () {
            $('#l10').show();
            $('#example_video_1').hide();
            myPlayer.pause();
        });
    </script>
@stop