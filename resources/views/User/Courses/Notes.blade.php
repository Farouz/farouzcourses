@extends('User.layout.master')
@section('Title')
    التنبيهات
    @stop

@section('content')
    <div class="corse-box col-xs-12">
        <div class="corse-nav text-center">
            <div class="container">
                <ul>
                    <li>
                        <a href="{{route('GET_THIS_COURSE',$course->id)}}">
                            <i class="fa fa-tasks"></i> الدروس
                        </a>
                    </li>

                    <li>
                        <a href="{{route('GET_COURSE_DISCUSS',$course->id)}}">
                            <span class="padge">{{\App\Discuss::count()}}</span>
                            <i class="fa fa-commenting-o"></i> النقاشات
                        </a>
                    </li>

                    <li>
                        <a href="#" class="active">
                            <span class="padge">{{\App\Note::count()}}</span>
                            <i class="fa fa-bell"></i> التنويهات
                        </a>
                    </li>
                </ul>
            </div>
            <!-- end container -->
        </div>
        <!-- end corse-nav -->
        <div class="lesson-box text-right">
            <div class="container">
                @if(count($notes)==null)

                <div class="empty-msg text-center animated shake">
                        <h1>
                            <i class="fa fa-frown-o"></i>
                            لا يوجد دروس الان ولكن يمكنك الاشتراك في الدورة لحين بدأها
                        </h1>
                    </div>
                    @else

                @foreach($notes as $note)
                <div class="alert-box">
                    <div class="all-alerts col-xs-12 text-right">
                        <ul>

                            <li>
                                <h1>{{$note->note_title}}</h1>
                                <span>{{$note->created_at->toDateString()}}</span>
                                <p>{{$note->note_description}}</p>
                            </li>

                        </ul>
                    </div>
                    <!-- end empty-msg -->
                </div>
            @endforeach
               @endif
            </div>
            <!-- end container -->
        </div>
        <!-- end lesson-box -->
    </div>

    @stop