@extends('User.layout.master')
@section('Title')
    المجالات
    @stop
@section('content')

    <div class="allcourses-box">
        <div class="allcourses-head text-center">
            <div class="container">
                <h1>{{ $category->title_ar}}</h1>

            </div>
            <!-- /.container -->
        </div>
        <!-- /.allcourses-head -->
        <div class="search-categories text-center">
            <div class="container">
                <div class="cat-item">
                    <div class="cat-inner col-md-6 col-sm-6 col-xs-6 pull-right">
                        <a href="#" class="show-cat">{{ $category->title_ar}} <i class="fa fa-caret-down"></i></a>
                        <div class="hidden-cat">
                            <ul>
                                @foreach($categories as $item)
                                <li>

                                    <a href='{{route('GET_CATEGORY',['id'=>$item->id])}}'>{{$item->title_ar}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /. cat-inner -->
                    <div class="cat-inner col-md-6 col-sm-6 col-xs-6 pull-left">
                        <form>
                            <input type="search" placeholder="ابحث عن كورسات أخري">
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                    <!-- /. cat-inner -->
                </div>
                <!-- /.cat-item -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.search-categories -->
        <div class="allcourses-body">
            <div class="container">
                <div class="row">
                    <div class="row block-container">

                        @foreach($courses as $cours)
                        <div class="block col-md-4">
                            <figure>
                                <div><img src="{{asset('User/images/'.$cours->course_image)}}" alt="img05" class="img-responsive"></div>
                                <figcaption class="text-right">
                                    <h1>اسم الكورس : {{$cours->title_ar}}</h1>
                                    <h1>اسم المدرس : {{\App\User::where('id',$cours->teacher_id)->first()->fullName}}</h1>
                                    <h1> :عدد الطلبة المشتركة :  <?php
                                        $students = \Illuminate\Support\Facades\DB::table('courses_users')->select('user_id')->where('course_id', $cours->id)->count();
                                        echo $students;
                                        ?>عدد الطلبة المشتركة</h1>
                                    <h1>تاريخ بدايه الكورس : {{$cours->course_date}}</h1>
                                    <h1>تقييم الكورس</h1>
                                    <a href="{{route('GET_THIS_COURSE',$cours->id)}}">
                                        <i class="fa fa-eye"></i> مشاهدة الكورس
                                    </a>
                                </figcaption>
                            </figure>
                        </div>
                        <!-- /.block -->
                        @endforeach

                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.row -->

                <div class="inner col-xs-12 text-center">
                    <ul class="pagination">
                        <ul class="pagination">
                            <li>
                                {{ $courses->links() }}
                            </li>
                        </ul>
                    </ul>
                </div>
                <!-- end inner -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /.allcourses-body -->
    </div>


    @stop