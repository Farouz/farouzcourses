@extends('User.layout.master')
@section('Title')
    About
    @stop
@section('content')
    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>معلومات عن الموقع</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box about-box">
            <div class="container">
                <div class="about-img col-md-4 col-xs-12 pull-left">
                    @foreach($settings as $setting)
                    <img src="{{asset('Public/User/images/'.$setting->about_image)}}" class="img-responsive" alt="">
                        @endforeach
                </div>
                <!-- end about-img -->
                <div class="about-data col-md-8 col-xs-12 pull-right text-right">
                    @foreach($settings as $setting)
                    <p>
                        {{$setting->description}}
                    </p>
                        @endforeach
                </div>
                <!-- end about-data -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>

    @stop
