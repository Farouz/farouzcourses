@extends('Dashboard.layout.master')
@section('content')
    <div class="contact-box">
        <span style="font-size: larger"> اسم المرسل له :</span><span style="font-size: large" > {{$message->sender_name}}</span>
       <br>
       <br>
       <br>
        <span style="font-size: larger"> البريد الاليكتروني  :</span><span style="font-size: large" > {{$message->sender_email}}</span>

    </div>
    <span></span>
    <span></span>
    <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
    <form method="post" action="{{route('POST_REPLY_MESSAGE')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="email" value="{{$message->sender_email}}">
        <textarea name="title"></textarea>
        <script>
            CKEDITOR.replace( 'title' );
        </script>
        <input type="submit" class="btn btn-success" value="Send">
    </form>
    @stop