@extends('Dashboard.layout.master')
@section('content')
    <h4>ALL Messages</h4>
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">All Messages</h3>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>اسم المرسل</th>
                        <th>الايميل</th>
                        <th>الرساله</th>
                        <th>Control</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $message)
                        <tr>
                            <td>{{$message->sender_name}}</td>
                            <td>{{$message->sender_email}}</td>
                            <td>{{$message->message}}</td>
                            <td>
                                <a href="{{route('GET_DELETE_MESSAGE',$message->id)}}"><i class="fa fa-trash fa-lg"
                                                                                          data-toggle="tooltip"
                                                                                          style="color: red"
                                                                                          title="Delete "></i></a><a
                                        href="{{route('GET_REPLY_MESSAGE',$message->id)}}"><i
                                            class="fa fa-mail-reply" data-toggle="tooltip" style="color: forestgreen"
                                            title="REPLY"></i></a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>
            </div><!-- /.box-body -->
        </div>
    </section>
@stop

@section('scripts')
    <script src="{{asset('plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.4 -->
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('plugins/fastclick/fastclick.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/app.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@stop