@extends('Dashboard.layout.master')
@section('content')
    <form method="post" action="{{route('POST_EDIT_SETTINGS',$setting->id)}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <i class="fa fa-university"></i>
            <label for="site_name">Organization Name</label>
            <input type="text" class="form-control" id="site_name" value="{{$setting->site_name}}"
                   name="site_name">
        </div>
        <div class="form-group">
            <i class="fa fa-image"></i>
            <label for="site_image">Organization Logo</label>
            <span class="btn btn-success btn-file">

                         Browse <input type="file" id="site_image" name="site_image" value="{{$setting->site_image}}">
                    </span>
        </div>
        <div class="form-group">
            <i class="fa fa-image"></i>
            <label for="about_image">About Image</label>
            <span class="btn btn-success btn-file">

                         Browse <input type="file" id="about_image" name="about_image" value="{{$setting->about_image}}">
                    </span>
        </div>
        <div class="form-group">
            <i class="fa fa-facebook-official"></i>
            <label for="facebook">Facebook Link</label>
            <input type="text" class="form-control" id="facebook" value="{{$setting->facebook}}"
                   name="facebook">
        </div>
        <div class="form-group">
            <i class="fa fa-twitter-square"></i>
            <label for="twitter">Twitter Link</label>
            <input type="text" class="form-control" id="twitter" value="{{$setting->twitter}}"
                   name="twitter">
        </div>
        <div class="form-group">
            <i class="fa fa-linkedin-square"></i>
            <label for="linkden">Linkden Link</label>
            <input type="text" class="form-control" id="linkden" value="{{$setting->linkden}}"
                   name="linkden">
        </div>
        <div class="form-group">
            <i class="fa fa-file-text"></i>
            <label for="description">Organization Description</label>
            <textarea class="form-control" id="description" name="description">{{$setting->description}}</textarea>
        </div>
        <div class="form-group">
            <i class="fa fa-slack"></i>
            <label for="policity">Organization Policy</label>
            <textarea class="form-control" id="policity" name="policity">{{$setting->policity}}</textarea>
        </div>


        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    @stop