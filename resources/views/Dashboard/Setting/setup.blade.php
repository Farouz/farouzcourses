@extends('Dashboard.layout.master')
@section('content')
    <h3>Setup Your Settings</h3>
    <hr>
    <form method="post" action="{{route('POST_ADD_SETTINGS')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <i class="fa fa-university"></i>
            <label for="site_name">Organization Name</label>
            <input type="text" class="form-control" id="site_name" placeholder="Organization Name .. "
                   name="site_name">
        </div>
        <div class="form-group">
            <i class="fa fa-image"></i>
            <label for="site_image">Organization Logo</label>
            <span class="btn btn-success btn-file">

                         Browse <input type="file" id="site_image" name="site_image">
                    </span>
        </div>
        <div class="form-group">
            <i class="fa fa-image"></i>
            <label for="about_image">About Image</label>
            <span class="btn btn-success btn-file">

                         Browse <input type="file" id="about_image" name="about_image">
                    </span>
        </div>
        <div class="form-group">
            <i class="fa fa-facebook-official"></i>
            <label for="facebook">Facebook Link</label>
            <input type="text" class="form-control" id="facebook" placeholder="FaceBook Account"
                   name="facebook">
        </div>
        <div class="form-group">
            <i class="fa fa-twitter-square"></i>
            <label for="twitter">Twitter Link</label>
            <input type="text" class="form-control" id="twitter" placeholder="Twitter Account"
                   name="twitter">
        </div>
        <div class="form-group">
            <i class="fa fa-linkedin-square"></i>
            <label for="linkden">Linkden Link</label>
            <input type="text" class="form-control" id="linkden" placeholder="Linkden Account"
                   name="linkden">
        </div>
        <div class="form-group">
            <i class="fa fa-file-text"></i>
            <label for="description">Organization Description</label>
            <textarea class="form-control" name="description" placeholder="About Organization"></textarea>
        </div>
        <div class="form-group">
            <i class="fa fa-slack"></i>
            <label for="policity">Organization Policy</label>
            <textarea class="form-control" name="policity" placeholder="Organization Policy"></textarea>
        </div>


        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@stop