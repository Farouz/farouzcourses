<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Dashboard De Farouz</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <link href="{{asset('User/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/font-awesome.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.theme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/selectric.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/reset.css')}}" rel="stylesheet" type="text/css">
    <!-- Font Awesome -->
    @if(\App::isLocale('ar'))
        <link rel="stylesheet" href="{{asset('dist/css/bootstrap-rtl.min.css')}}">
    @endif
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">
@yield('CssFiles')
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">


        <!-- Logo -->
        <a href="{{route('Dashboard')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>LT</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>@lang('alert.Dashboard')</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- Default image -->
                            <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{Auth::guard('webadmin')->user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="{{route('GET_ADMIN_LOGOUT')}}" class="btn btn-default btn-flat">Sign
                                        out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{Auth::guard('webadmin')->user()->name}}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                            class="fa fa-search"></i></button>
              </span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>@lang('alert.lang')</span> <i
                                class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('CHANGE_LANGUAGE','en')}}"><i class="fa fa-circle-o"></i> DASHBOARD ENGLISH</a>
                        </li>
                        <li class="active"><a href="{{route('CHANGE_LANGUAGE','ar')}}"><i class="fa fa-circle-o"></i>لوحه
                                التحكم</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>@lang('alert.Admins')</span>
                        <span class="label label-primary pull-right"></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('GET_ALL_ADMINS')}}"><i class="fa fa-users"></i> @lang('alert.ALLAdmins')</a></li>
                        <li><a href="{{route('GET_ADD_ADMIN')}}"><i class="fa fa-user-plus"></i> @lang('alert.ADDAdmins')</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-files-o"></i>
                        <span>@lang('alert.CoursesCategory')</span>
                        <span class="label label-primary pull-right"></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('GET_ADD_CATEGORY')}}"><i class="fa fa-circle-o"></i> Add Category</a></li>
                        <li><a href="{{route('GET_ALL_CATEGORIES')}}"><i class="fa fa-circle-o"></i> ALL Category</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>USERS</span>
                        <span class="label label-primary pull-right"></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('GET_ALL_USERS')}}"><i class="fa fa-users"></i> ALL USERS</a>
                        </li>
                        <li><a href="{{route('GET_ADD_USER')}}"><i class="fa fa-user-plus"></i> ADD USER</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-book"></i>
                        <span>COURSES</span>
                        <span class="label label-primary pull-right"></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('GET_ALL_COURSES_DASH')}}"><i class="fa fa-book"></i> ALL COURSES</a>
                        </li>
                        <li><a href="{{route('GET_ADD_COURSE_DASH')}}"><i class="fa fa-plus-circle"></i> ADD COURSES</a>
                        </li>
                        <li><a href="{{route('GET_ALL_LECTURES_DASH')}}"><i class="fa fa-caret-square-o-left"></i> ALL
                                LECTURES</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-gear"></i>
                        <span>SETTINGS</span>
                        <span class="label label-primary pull-right"></span>
                    </a>
                    <ul class="treeview-menu">
                        @if(App\Setting::count()== 0)
                            <li><a href="{{route('GET_SETUP_SETTINGS')}}"><i class="fa fa-gears"></i> SETUP SETTINGS</a>
                            </li>
                        @else
                            <li><a href="{{route('GET_SHOW_SETTINGS')}}"><i class="fa fa-gears"></i> SHOW SETTINGS</a>
                            </li>
                        @endif
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>CUSTOMERS</span>
                        <span class="label label-primary pull-right"></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('GET_ADD_CUSTOMERS')}}"><i class="fa fa-user-plus"></i>ADD CUSTOMERS</a>
                        </li>
                        <li><a href="{{route('GET_ALL_CUSTOMERS')}}"><i class="fa fa-users"></i>ALL CUSTOMERS</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-inbox"></i>
                        <span>MESSAGES</span>
                        <span class="label label-primary pull-right"></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('GET_ALL_Messages')}}"><i class="fa fa-inbox"></i>ALL MESSAGES</a>
                        </li>

                    </ul>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content">
            @include('User.layout.messages')
            @yield('content')
        </section>

    </div>
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.2.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
        reserved.
    </footer>

    <!-- Control Sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->

</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="{{asset('plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
<!-- Bootstrap 3.3.4 -->
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/app.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{asset('plugins/chartjs/Chart.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard2.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>

@yield('scripts')
</body>
</html>
