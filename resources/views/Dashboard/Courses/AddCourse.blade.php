@extends('Dashboard.layout.master')
@section('CssFiles')

    <link href="{{asset('User/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/font-awesome.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.theme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/selectric.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/reset.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/images/favicon.png')}}" rel="icon" type="text/css">

@stop
@section('content')
    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>إضافه دوره جديده </h1>

            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form">

                    <form action="{{route('POST_ADD_COURSE_DASH')}}" method="post" class="add-form"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="up_form-item">
                            <h1>عنوان الدورة</h1>
                            <input type="text" name="course_title" placeholder="اسم الدوره">
                        </div>
                        <div class="up_form-item">
                            <h1>المدرب</h1>
                            <select name="teacher_id">
                                @foreach($teachers as $teacher)
                                    <option value="{{$teacher->id}}">{{$teacher->fullName}} &nbsp; as &nbsp;
                                        &nbsp;{{$teacher->userName}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="up_form-item">
                            <h1>تاريخ بدايه الدوره</h1>
                            <input type="date" name="course_date" class="form-control" placeholder="تاريخ بدايه الدوره">
                        </div>
                        <div class="up_form-item">
                            <h1>تاريخ انتهاء الدوره</h1>
                            <input type="date" name="course_end" class="form-control" placeholder="تاريخ نهايه الدوره">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>متطلب سابق</h1>
                            <input type="text" name="pre_exp" class="form-control" placeholder="متطلب سابق">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>المجال</h1>
                            <select name="cat_id">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->title_ar}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="up_form-item">
                            <h1> رابط الفيديو والصوره </h1>
                            <div class="add_cont text-right">
                                <div class="lecture-item">
                                    <div class="add_cont text-right">
                                        <label class="text-right">
                                            <input type="checkbox" id="up-video">
                                            <span>اذا أردت رفع فيديو وصوره من جهازك الشخصي</span>
                                        </label>

                                        <div class="videoUploaded col-xs-12 text-right">
                                            <span><i class="fa fa-video-camera"></i> ارفع فيديو من جهازك</span>
                                            <input type="file" name="course_video" class="uploaded">
                                        </div>
                                        <div class="videoUploaded col-xs-12 text-right">
                                            <span><i class="fa fa-camera"></i> ارفع صوره من جهازك</span>
                                            <input type="file" name="course_image" class="uploaded">
                                        </div>

                                    </div>

                                </div>
                                <!-- /.lecture-item -->
                            </div>
                            <input type="text" name="course_video_url" placeholder="ادخل رابط فيديو" class="linked" aria-placeholder="just copy youtube urls not embeded">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>وصف الدورة</h1>
                            <textarea name="course_description" placeholder="اضافه معلومات عن الدوره"></textarea>
                        </div>


                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <h1>الجنس المتوقع</h1>
                            <div class="add_cont text-right">
                                <label class="text-right">
                                    <input type="checkbox" value="1" name="gender">
                                    <span>ذكور</span>
                                </label>
                                <label class="text-right">
                                    <input type="checkbox" value="2" name="gender">
                                    <span>إناث</span>
                                </label>
                            </div>
                        </div>
                        <div class="up_form-item">
                            <h1>نوع الدورة</h1>
                            <div class="add_cont text-right">
                                <label class="text-right">
                                    <input type="radio" value="1" name="course_payment_method" id="ch_1"
                                           onclick="javascript:isCheck();">
                                    <span>مجاني</span>
                                </label>
                                <label class="text-right">
                                    <input type="radio" value="2" name="course_payment_method" id="ch_2"
                                           onclick="javascript:isCheck();">
                                    <span>مدفوع</span>
                                </label>
                                <input type="number" id="ch_salary" name="course_salary" data-toggle="tooltip"
                                       data-placement="top"
                                       title="اضف سعر الدورة">
                            </div>
                        </div>
                        <!-- /.up_form-item -->

                        <div class="up_form-item">
                            <a class="add-cert">اضافة شهادة للدورة</a>
                            <div class="course-cert">
                                <div class="up_form-item">
                                    <h1>إسم الشهادة</h1>
                                    <input type="text" name="certificate_name" placeholder="اسم الشهاده">
                                </div>
                                <!-- /.up_form-item -->
                                <div class="up_form-item">
                                    <h1>الجهة المانحة</h1>
                                    <input type="text" name="certificate_branch" placeholder="الجهه المانحه">
                                </div>
                                <!-- /.up_form-item -->
                                <div class="up_form-item">
                                    <h1>تكلفة الشهادة</h1>
                                    <div class="add_cont text-right">
                                        <label class="text-right">
                                            <input type="radio" value="1" name="certificate_payment" id="certi_1"
                                                   onclick="javascript:certiCheck();">
                                            <span>مجاني</span>
                                        </label>
                                        <label class="text-right">
                                            <input type="radio" value="2" id="certi_2"
                                                   onclick="javascript:certiCheck();"
                                                   name="certificate_payment">
                                            <span>مدفوع</span>
                                        </label>
                                        <input type="number" id="certi_salary" name="certificate_salary"
                                               data-toggle="tooltip"
                                               data-placement="top"
                                               title="اضف سعر الشهادة"></div>
                                </div>
                                <!-- /.up_form-item -->
                            </div>
                            <!-- /.course-cert -->
                        </div>
                        <!-- /.up_form-item -->

                        <div class="up_form-item up-confirm">
                            <input type="submit" value="اضافة الدورة">
                        </div>
                        <!-- /.up_form-item -->
                    </form>

                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>




@stop
<script src="{{asset('User/js/jquery-2.2.2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/html5shiv.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/jquery-smoothscroll.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/modernizr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/wow.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/placeholdem.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/toucheffects.js')}}"></script>
<script src="{{asset('User/js/jquery.selectric.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/classie.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/jquery.nicescroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/script.js')}}" type="text/javascript"></script>

<script type="text/javascript">

    function isCheck() {
        if (document.getElementById('ch_2').checked) {
            document.getElementById('ch_salary').style.visibility = 'visible';
        }
        else {
            document.getElementById('ch_salary').style.visibility = 'hidden';
        }
    }

    function certiCheck() {
        if (document.getElementById('certi_2').checked) {
            document.getElementById('certi_salary').style.visibility = 'visible';
        }
        else {
            document.getElementById('certi_salary').style.visibility = 'hidden';
        }
    }

</script>