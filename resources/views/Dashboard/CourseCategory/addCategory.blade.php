@extends('Dashboard.layout.master')
@section('content')
    <h3>Add New Course Category</h3>
    <form method="post" action="{{route('POST_ADD_CATEGORY')}}" enctype="multipart/form-data" >
        {{csrf_field()}}
        <div class="form-group">
            <label for="catTitle">Category Title</label>
            <input type="text"  name="title_en" class="form-control" id="catTitle" placeholder="Category Title">
        </div>
        <div class="form-group">
            <label for="CatTitleAr">اسم المجال</label>
            <input type="text" name="title_ar" class="form-control" id="CatTitleAr" placeholder="اسم المجال">
        </div>
        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
    </form>
    @stop