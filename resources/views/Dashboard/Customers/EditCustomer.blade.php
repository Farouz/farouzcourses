@extends('Dashboard.layout.master')
@section('content')
    <form method="post" action="{{route('POST_EDIT_CUSTOMER',$customer->id)}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <i class="fa fa-user"></i>
            <label for="name">Customer Name</label>
            <input type="text" class="form-control" id="name" value="{{$customer->name}}"
                   name="name">
        </div>
        <div class="form-group">
            <i class="fa fa-image"></i>
            <label for="customer_image">Customer Image</label>
            <span class="btn btn-success btn-file">

                         Browse <input type="file" id="customer_image" name="image" value="{{$customer->image}}">
                    </span>
        </div>


        <div class="form-group">
            <i class="fa fa-slack"></i>
            <label for="desc">What he say ! ..</label>
            <textarea class="form-control" name="desc">{{$customer->desc}}</textarea>
        </div>


        <button type="submit" class="btn btn-primary">Submit</button>
    </form>


@stop