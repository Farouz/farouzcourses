@extends('Dashboard.layout.master')
@section('content')
    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>تعديل حساب
                    {{$user->fullName}}
                </h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form">

                    <form action="{{route('POST_EDIT_USER',$user->id)}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="up_form-item">
                            <span id="error-form">من فضلك ادخل البيانات الصحيحة</span>
                            <input type="text" name="fullName" value="{{$user->fullName}}">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="text" name="userName" value="{{$user->userName}}">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="text" name="email" value="{{$user->email}}">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="password" name="password" value="{{$user->password}}">
                        </div><!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="text" name="phone" value="{{$user->phone}}">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <span style="float: right;">صوره العضو</span>
                            <label class="btn btn-default btn-file">
                                <input type="file" style="display: none; min-width: 100%" name="image"
                                       value="{{$user->Image}}"> BROWSE
                            </label>
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="text" name="qualification"
                                   value="{{$user->qualification}}">
                        </div>
                        <div class="up_form-item">
                            <input type="text" name="spcialization"
                                   value="{{$user->spcialization}}">
                        </div>
                        <div class="up_form-item">
                            <input type="text" name="job_title" placeholder="المهنه" value="{{$user->job_title}}">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <select name="country">
                                <option value="{{$user->country}}">{{$user->country}}</option>
                                @foreach($countries as $country)
                                    <option value="{{$country->country_name}}">{{$country->country_name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <select name="gender">
                                <option value="{{$user->gender}}"><?php if ($user->gender == 1) {
                                        echo 'مذكر';
                                    } else
                                        echo 'مؤنث';
                                    ?></option>
                                <option value="1">مذكر</option>
                                <option value="2">مؤنث</option>
                            </select>
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item text-right">
                            <label>
                                <input name="is_teacher" value="1" type="checkbox" @if($user->is_teacher==1)
                                checked
                                        @endif>
                                <span>مدرب</span>
                            </label>
                            <label>
                                <input name="is_trainee" value="0" type="checkbox"
                                       @if($user->is_teacher == 0)
                                       checked
                                        @endif>
                                <span>متدرب</span>
                            </label>
                        </div>

                        <div class="up_form-item up-confirm">
                            <input type="submit" value="تسجيل">
                        </div>
                        <!-- /.up_form-item -->
                    </form>

                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>


@stop