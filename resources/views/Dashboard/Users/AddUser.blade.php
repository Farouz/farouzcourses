@extends('Dashboard.layout.master')
@section('content')
    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>يرجي تسجيل حساب جديد</h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form">

                    <form action="{{route('POST_ADD_USER')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="up_form-item">
                            <span id="error-form">من فضلك ادخل البيانات الصحيحة</span>
                            <input type="text" name="fullName" placeholder="الإسم بالكامل">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="text" name="userName" placeholder="إسم المستخدم">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="text" name="email" placeholder="البريد الإلكتروني">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="password" name="password" placeholder="كلمة المرور">
                        </div><!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="text" name="phone" placeholder=" رقم التليفون">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <span style="float: right;">صوره العضو</span>
                            <label class="btn btn-default btn-file">
                                <input type="file" style="display: none; min-width: 100%" name="image"> BROWSE
                            </label>
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <input type="text" name="qualification" placeholder="المؤهل الدراسي">
                        </div>
                        <div class="up_form-item">
                            <input type="text" name="spcialization" placeholder="التخصص">
                        </div>
                        <div class="up_form-item">
                            <input type="text" name="job_title" placeholder="المهنه">
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <select name="country">
                                <option>الدولة ...</option>
                                @foreach($countries as $country)
                                    <option value="{{$country->country_name}}">{{$country->country_name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item">
                            <select name="gender">
                                <option>الجنس ...</option>
                                <option value="1">مذكر</option>
                                <option value="2">مؤنث</option>
                            </select>
                        </div>
                        <!-- /.up_form-item -->
                        <div class="up_form-item text-right">
                            <label>
                                <input name="is_teacher" value="1" type="checkbox">
                                <span>مدرب</span>
                            </label>
                            <label>
                                <input name="is_trainee" value="0" type="checkbox">
                                <span>متدرب</span>
                            </label>
                        </div>

                        <div class="up_form-item up-confirm">
                            <input type="submit" value="تسجيل">
                        </div>
                        <!-- /.up_form-item -->
                    </form>

                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>


@stop