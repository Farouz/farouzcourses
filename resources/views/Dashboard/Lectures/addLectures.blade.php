@extends('Dashboard.layout.master')
@section('CssFiles')

    <link href="{{asset('User/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/font-awesome.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/owl.theme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/selectric.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/css/reset.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('User/images/favicon.png')}}" rel="icon" type="text/css">
@stop
@section('content')

    <div class="up-container">
        <div class="up-header text-center">
            <div class="container">
                <h1>إضافة محاضرة جديدة خاصه كورس
                    <br>
                    {{$course->course_title}}

                </h1>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-header -->
        <div class="up-box">
            <div class="container">
                <div class="up-form">

                    <div class="add_lecture in-one">
                        <form action="{{route('POST_ADD_LECTURE_DASH')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="course_id" value="{{$course->id}}">
                            <input type="hidden" name="teacher_id" value="{{$teacher_id}}">
                            <div class="lecture-item">
                                <h1>اسم المحاضره </h1>
                                <input type="text" name="lecture_title" placeholder="اضف اسم المحاضرة" required>
                            </div>
                            <!-- /.lecture-item -->
                            <div class="lecture-item">
                                <h1>اضف رابط خارجي للفيديو</h1>
                                <div class="add_cont text-right">
                                    <label class="text-right">
                                        <input type="checkbox" id="up-video">
                                        <span>اذا أردت رفع فيديو من جهازك الشخصي</span>
                                    </label>

                                    <div class="videoUploaded col-xs-12 text-right">
                                        <span><i class="fa fa-video-camera"></i> ارفع فيديو من جهازك</span>
                                        <input type="file" name="lecture_video" class="uploaded">
                                    </div>
                                </div>
                                <input type="text" name="lecture_url" placeholder="ادخل رابط فيديو" class="linked">
                                <small>just put the url</small>
                            </div>
                            <div class="lecture-item">
                                <h1>مده المحاضره </h1>
                                <input type="number" name="lecture_time" class="form-control" required>
                            </div>
                            <div class="lecture-item">
                                <h1>ترتيب المحاضره </h1>
                                <input type="number" name="order" class="form-control" required>
                            </div>
                            <!-- /.lecture-item -->
                            <div class="lecture-item">
                                <h1>وصف المحاضره  </h1>
                                <textarea name="lecture_description" placeholder="اضف وصف المحاضرة"></textarea>
                            </div>
                            <!-- /.lecture-item -->
                            <div class="lecture-item text-right">
                                <div class="fileUpload col-xs-12 text-right">
                                    <span><i class="fa fa-file"></i> رابط أوراق العمل </span>
                                    <input type="file" name="lecture_file" class="upload">
                                </div>
                                <span class="hint"> Image او Word او Powerpoint او Pdf الملفات يمكن ان تكون </span>
                            </div>
                            <!-- /.lecture-item -->
                            <div class="lecture-item confirm-lec">
                                <input type="submit" value="إضافة محاضرة">
                            </div>
                            <!-- /.lecture-item -->

                        </form>
                    </div>
                    <!-- /.add_lecture -->
                </div>
                <!-- /.up-form -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.up-box -->
    </div>

    @stop

<script src="{{asset('User/js/jquery-2.2.2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/html5shiv.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/jquery-smoothscroll.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/modernizr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/wow.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/placeholdem.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/toucheffects.js')}}"></script>
<script src="{{asset('User/js/jquery.selectric.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/classie.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/jquery.nicescroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('User/js/script.js')}}" type="text/javascript"></script>