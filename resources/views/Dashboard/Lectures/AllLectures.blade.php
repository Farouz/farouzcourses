@extends('Dashboard.layout.master')
@section('content')
    <h4>All Lectures</h4>
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">All Lectures</h3>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>أسم المحاضره</th>
                        <th>أسم الدوره</th>
                        <th>اسم المدرس</th>
                        <th>حاله التصريح</th>
                        <th>التحكم</th>
                    </tr>
                    </thead>
                    @foreach($lectures as $lecture)
                        <tbody>

                        <td>{{$lecture->lecture_title}}</td>
                        <td>{{App\Course::where('id',$lecture->course_id)->first()->course_title}}</td>
                        <td>{{App\User::where('id',$lecture->teacher_id)->first()->fullName}} as {{App\User::where('id',$lecture->teacher_id)->first()->userName}}</td>

                        <td>@if($lecture->approved == 1)
                     تم التصريح
                        @else لم يتم التصريح
                        @endif
                        </td>
                        <td>
                            <a href="{{route('GET_DELETE_LECTURE_DASH',$lecture->id)}}">
                                <i class="fa fa-trash fa-lg" style="color: red" data-toggle="tooltip"
                                   title="Delete Lecture "></i>
                            </a>
                            </a><a href="{{route('GET_LECTURE_DETAILS_DASH',$lecture->id)}}">
                                <i class="fa fa-desktop" style="color: brown" data-toggle="tooltip"
                                   title="Show Lecture Details "></i>
                            </a>
                            </a></a><a href="{{route('GET_EDIT_LECTURE_DASH',$lecture->id)}}">
                                <i class="fa fa-edit" style="color: brown" data-toggle="tooltip"
                                   title="Edit Lecture"></i>
                            </a>
                            @if($lecture->approved == 0)
                                <a href="{{route('APPROVE_LECTURE',$lecture->id)}}">
                                    <i class="fa fa-check-square fa-lg" style="color: green;" data-toggle="tooltip"
                                       title="Approve The Lecture"></i>
                                </a>
                            @endif
                        </td>
                        </tbody>
                    @endforeach
                </table>
            </div><!-- /.box-body -->
        </div>
    </section>
@stop

@section('scripts')
    <script src="{{asset('plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.4 -->
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('plugins/fastclick/fastclick.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/app.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@stop