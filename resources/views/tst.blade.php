<form method="post" action="{{ url('/password/email') }}" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="panel-pop modal" id="forget">
        <div class="lost-inner">
            <h1>هل نسيت كلمة المرور ؟</h1>
            <div class="lost-item">
                <input type="text" name="email" placeholder="الايميل المستخدم في تسجيل الدخول">
            </div>
            <!-- /.lost-item -->
            <div class="text-center">
                <input type="submit" value="إعادة ضبط">
            </div>
            <!-- /.lost-item -->
        </div>
        <!-- /.lost-inner -->
    </div>
</form>
