<?php

namespace App\Http\Controllers;

use App\Course;
use App\Lecture;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DashLecturesController extends Controller
{
    public function getAddLectures($id)
    {
        $course = Course::find($id);
        $teacher_id = User::where('id', $course->teacher_id)->first()->id;
        return view('Dashboard.Lectures.addLectures', compact('course', 'teacher_id'));
    }

    public function postAddLectures(Request $request)
    {
        $this->validate($request, ['lecture_title' => 'required|regex:/^([^0-9]*)$/', 'lecture_video' => 'mimes:mp4,webm,flv,mkv,avi | max:20000', 'lecture_url' => 'required_if:course_video,null', 'lecture_description' => 'required|regex:/^([^0-9]*)$/', 'lecture_file' => 'mimes:pdf,docs,docx | max:20000', 'lecture_time' => 'required|numeric|min:0', 'order' => 'numeric|min:0|nullable']);
        $lecture = new Lecture();

        $lecture->course_id = $request->course_id;

        $lecture->lecture_title = $request->lecture_title;

        $lecture->lecture_url = $request->lecture_url;

        $lecture->lecture_time = $request->lecture_time;

        $lecture->teacher_id = $request->teacher_id;

        $lecture->lecture_description = $request->lecture_description;

        if ($request->hasFile('lecture_file')) {

            $file_name = md5($request->lecture_file->getClientOriginalName() . '.' . $request->lecture_file->getClientOriginalExtension());

            $request->lecture_file->move(public_path('User/Files'), $file_name);

            $lecture->lecture_file = $file_name;

        }

        if ($request->hasFile('lecture_video')) {

            $lecture_video = md5($request->lecture_video->getClientOriginalName() . ',' . $request->lecture_video->getClientOriginalExtension());

            $request->lecture_video->move(public_path('User/Videos'), $lecture_video);

            $lecture->lecture_video = $lecture_video;

        }


        if (isset($lecture->order)) {

            $lecture->order = $request->order + 1;

        } else {


            $lecture->order = $request->order;

        }

        $lecture->approved = 1;

        $lecture->save();

        return redirect()->route('GET_ALL_COURSES_DASH')->with('success', 'Lecture Added');
    }

    public function getAllLectures()
    {
        $lectures = Lecture::orderBy('order', 'ASC')->get();
        return view('Dashboard.Lectures.AllLectures', compact('lectures'));
    }

    public function getrelatedLeture($id)
    {
        $course = Course::findOrFail($id);
        $lectures = Lecture::where('course_id', $id)->get();
        return view('Dashboard.Lectures.SingleCourse', compact('lectures', 'course'));
    }

    public function getThisLectureDetails($id)
    {
        $lecture = Lecture::find($id);
        return view('Dashboard.Lectures.Details', compact('lecture'));
    }

    public function getEditLecture($id)
    {
        $lecture = Lecture::findOrFail($id);
        $courses = Course::all();
        $teachers = User::where('is_teacher', 1)->get();
        return view('Dashboard.Lectures.Edit', compact('lecture', 'courses', 'teachers'));
    }

    public function postEditLecture($id, Request $request)
    {
        $this->validate($request, ['lecture_title' => 'required|regex:/^([^0-9]*)$/|string', 'lecture_video' => 'mimes:mp4,webm,flv,mkv,avi | max:20000', 'lecture_url' => 'required_if:course_video,null', 'lecture_description' => 'required|regex:/^([^0-9]*)$/|string', 'lecture_file' => 'mimes:pdf,docs,docx | max:20000', 'lecture_time' => 'required|numeric|min:0', 'order' => 'numeric|min:0|nullable']);
        $lecture = Lecture::find($id);
        if ($request->hasFile('lecture_file')) {

            $file_name = md5($request->lecture_file->getClientOriginalName() . '.' . $request->lecture_file->getClientOriginalExtension());

            $request->lecture_file->move(public_path('User/Files'), $file_name);
        } else {
            $file_name = $lecture->lecture_file;
        }

        if ($request->hasFile('lecture_video')) {

            $lecture_video = md5($request->lecture_video->getClientOriginalName() . ',' . $request->lecture_video->getClientOriginalExtension());

            $request->lecture_video->move(public_path('User/Videos'), $lecture_video);


        } else {
            $lecture_video = $lecture->lecture_video;
        }


        if (isset($lecture->order)) {

            $lecture->order = $request->order + 1;

        } else {


            $lecture->order = $request->order;

        }
        Lecture::where('id', $id)->update(['lecture_title' => $request->lecture_title, 'course_id' => $request->course_id, 'teacher_id' => $request->teacher_id, 'lecture_description' => $request->lecture_description, 'lecture_file' => $file_name, 'lecture_video' => $lecture_video, 'lecture_time' => $request->lecture_time, 'lecture_url' => $request->lecutre_url]);

        $lecture->save();
        return redirect()
            ->route('GET_LECTURE_DETAILS_DASH',$lecture->id)
            ->with('success','Lecture Edited');
    }

}
