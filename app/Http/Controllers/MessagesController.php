<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Mail\ContactMail;
use App\Reply;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;


class MessagesController extends Controller
{
    public function getAllMessages()
    {
        $messages = Contact::all();

        return view('Dashboard.Messages.AllMessages', compact('messages'));
    }

    public function deleteMessages($id)
    {
        Contact::destroy($id);
        return back()->with('success', 'Message Deleted');
    }

    public function getReplyMessage($id)
    {
        $message = Contact::find($id);
        return view('Dashboard.Messages.Reply', compact('message'));
    }

    public function postReplyMessage(Request $request, Mailer $mailer)
    {
        $message = new Reply();
        $message->recipt = $request->email;
        $message->message = strip_tags($request->title);
        $message->save();
        $mailer->to($request->email)->send(new ContactMail($request->email, $request->title));
        return redirect()->route('GET_ALL_REPLIES')->with('success', 'Mail Sent Successfully');
    }

    public function getAllreplies()
    {
        $msgs = Reply::all();
        return view('Dashboard.Messages.AllReplies', compact('msgs'));
    }

    public function getDeletereply($id)
    {
        Reply::destroy($id);
        return back()->with('success', 'deleted');
    }
}
