<?php

namespace App\Http\Controllers;

use App\Course;
use App\Lecture;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Setting;


class LecturesController extends Controller
{
    public function getAddLecture()
    {
        $settings = Setting::all();

        return view('User.Courses.AddLecture', compact('settings'));
    }

    public function postAddLecture(Request $request)
    {
        $this->validate($request, ['lecture_title' => 'required|regex:/^([^0-9]*)$/', 'lecture_video' => 'mimes:mp4,webm,flv,mkv,avi | max:20000', 'lecture_url' => 'required_if:course_video,null', 'lecture_description' => 'required|regex:/^([^0-9]*)$/', 'lecture_file' => 'mimes:pdf,docs,docx | max:20000']);
        $lecture = new Lecture();
        $lecture->course_id = $request->course_id;
        $lecture->lecture_title = $request->lecture_title;
        $lecture->lecture_url = $request->lecture_url;
        $lecture->lecture_description = $request->lecture_description;
        if ($request->hasFile('lecture_file')) {
            $file_name = md5($request->lecture_file->getClientOriginalName() . '.' . $request->lecture_file->getClientOriginalExtension());
            $request->lecture_file->move(public_path('User/Files'), $file_name);
            $lecture->lecture_file = $file_name;
        }
        if ($request->hasFile('lecture_video')) {
            $lecture_video = md5($request->lecture_video->getClientOriginalName() . ',' . $request->lecture_video->getClientOriginalExtension());
            $request->lecture_video->move(public_path('User/Videos'), $lecture_video);
            $lecture->lecture_video = $lecture_video;
        }
        $lecture->teacher_id = Auth::id();
        $last_lecture = DB::table('lectures')->min('id');
        if ($request->has('count')) {
            $count = $request->input('count');
        } else {
            $count = 0;
        }
        $lecture->id = $last_lecture + $count;
        $extist = $lecture->id;
        if (isset($extist)) {
            $lecture->id = $extist + 1;
        }
        $lecture->save();

//        $user_id = Auth::id();
//        $lecture->users()->attach($user_id);
        return back()->with('success', 'Lecture Added');
    }

    public function getAddLectures($id)
    {
        $settings = Setting::all();

        $course = Course::findOrFail($id);
        return view('User.Courses.AddLecture', compact('course', 'settings'));
    }

    public function getDeleteLecture($id)
    {
        Lecture::destroy($id);
        return back()->with('error', 'تم حذف المحاضره');
    }

    public function getSingleLecture($id)
    {
        $lecture = Lecture::findOrFail($id);
        $settings = Setting::all();

        $lectures = Lecture::orderBy('order', 'ASC')->get();
        return view('User.Courses.SingleLecture', compact('lecture', 'lectures', 'settings'));
    }

    public function getFinishCourse($id)
    {
        $lecture = Lecture::findOrFail($id);
        $user_id = Auth::id();
        if (!Auth::user()->Lectures()->where('lecture_id',$id)->exists()){
            $lecture->users()->attach($user_id);
        }
        return back();
    }


}
