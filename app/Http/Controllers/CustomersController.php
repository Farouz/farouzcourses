<?php

namespace App\Http\Controllers;

use App\Talk;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    public function getAddCustomers()
    {
        return view('Dashboard.Customers.AddCustomer');
    }

    public function postAddCustomers(Request $request)
    {
        $this->validate($request, ['name' => 'required|string', 'image' => 'required|image', 'desc' => 'required|string']);
        $customer = new Talk();
        $customer->name = $request->name;
        $customer->desc = $request->desc;
        if ($request->hasFile('image')) {
            $image_name = md5($request->image->getClientOriginalName()) . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('User/images'), $image_name);
            $customer->Image = $image_name;
        }
        $customer->save();
        return redirect()->route('GET_ALL_CUSTOMERS')->with('success', 'Customer Added');
    }

    public function getAllCustomers()
    {
        $customers = Talk::all();
        return view('Dashboard.Customers.allCustomers', compact('customers'));
    }

    public function getEditCustomer($id)
    {
        $customer=Talk::find($id);
        return view('Dashboard.Customers.EditCustomer',compact('customer'));
    }

    public function postEditCustomer($id, Request $request)
    {
        $this->validate($request, ['name' => 'required|string', 'image' => 'image', 'desc' => 'required|string']);
        $customer = Talk::find($id);
        if ($request->hasFile('image')) {
            $image_name = md5($request->image->getClientOriginalName()) . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('User/images'), $image_name);

        } else {
            $image_name = $customer->image;
        }
        Talk::where('id', $id)->update(['name' => $request->name, 'desc' => $request->desc, 'image' => $image_name]);
        $customer->save();
        return redirect()->route('GET_ALL_CUSTOMERS')->with('success', 'Customer Edited');
    }
}
