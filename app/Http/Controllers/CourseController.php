<?php

namespace App\Http\Controllers;

use App\Certificate;
use App\Course;
use App\CourseCategory;
use App\CourseUser;
use App\CV;
use App\Discuss;
use App\Lecture;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Setting;


class CourseController extends Controller
{
    //The Users Courses
    //get Add Courses Page
    public function getAddCourse()
    {
        //settings in all pages
        $settings = Setting::all();
        $categories = CourseCategory::all();
        return view('User.Courses.addCourse', compact('categories', 'settings'));
    }
//the post Add Course
    public function postAddCourse(Request $request)
    {
        $this->validate($request,
            [
                'course_title' => 'required|alpha_dash',
                'pre_exp' => 'alpha_dash',
                'cat_id' => 'required',
                'course_video' => 'mimes:mp4,webm,flv,mkv,avi | max:20000',
                'course_image' => 'image|max:20000',
                'course_video_url' => 'required_if:course_video,null',
                'course_description' => 'required|regex:/^([^0-9]*)$/',
                'gender' => 'required',
                'course_payment_method' => 'required',
                'course_salary' => 'required_if:course_payment_method,2',
                'course_date' => 'required|date',
                'course_end' => 'required|date|after:course_date',
                'certificate_name' => 'alpha_dash',
                'certificate_branch' => 'alpha_dash',
                'certificate_salary' => 'min:1|required_if:certificate_payment,2'
            ]);

        //Add Course and its properties

        $course = new Course();
        $course->course_title = $request->course_title;
        $course->pre_exp = $request->pre_exp;
        $course->course_date = $request->course_date;
        $course->course_end = $request->course_end;
        $course->cat_id = $request->cat_id;
        $course->course_video_url = $request->course_video_url;
        $course->course_description = $request->course_description;
        $course->course_salary = $request->course_salary;
        $course->course_payment_method = $request->course_payment_method;

        //Certification Details

        $course->certificate_name = $request->certificate_name;
        $course->certificate_branch = $request->certificate_branch;
        $course->certificate_payment = $request->certificate_payment;
        $course->certificate_salary = $request->certificate_salary;

        //getting the teacher id

        $teacher_id = Auth::id();
        $course->teacher_id = $teacher_id;
        if ($request->hasFile('course_image')) {
            $image_name = md5($request->course_image->getClientOriginalName()) . '.' . $request->course_image->getClientOriginalExtension();
            $request->course_image->move(public_path('User/images'), $image_name);
            $course->course_image = $image_name;
        } elseif (!$request->hasFile('course_image')) {
            $course->course_image = 'course_default.jpg';
        }
        if ($request->hasFile('course_video')) {
            $video_name = md5($request->course_video->getClientOriginalName()) . '.' . $request->course_video->getClientOriginalExtension();
            $request->image_video->move(public_path('User/uploads/Videos'), $video_name);
            $course->course_video = $video_name;
        }
        if ($request->has('gender') == 1) {
            $course->gender = $request->gender;
        } elseif ($request->has('gender' == 2)) {
            $course->gender = $request->gender;
        } elseif ($request->has('gender') == 1 && $request->has('gender') == 2) {
            $course->gender = 3;
        }

        $course->save();
        return back()->with('success', 'Course Added Successfully');
    }

    //Edit The Course Post

    public function postEditCourse($id, Request $request)
    {
        $this->validate($request, [
            'course_title' => 'regex:/^([^0-9]*)$/',
            'pre_exp' => 'regex:/^([^0-9]*)$/',
            'cat_id' => 'required',
            'course_video' => 'mimes:mp4,webm,flv,mkv,avi | max:20000',
            'course_image' => 'image|max:20000',
            'course_video_url' => 'required_if:course_video,null',
            'course_description' => 'required|regex:/^([^0-9]*)$/',
            'gender' => 'required',
            'course_payment_method' => 'required',
            'course_salary' => 'min:1|required_if:course_payment_method,2',
            'course_date' => 'required|date',
            'course_end' => 'required|date|after:course_date',
            'certificate_name' => 'alpha_dash',
            'certificate_branch' => 'alpha_dash',
            'certificate_salary' => 'min:1|required_if:certificate_payment,2'
        ]);
        //Catching the Course To Update
        $course = Course::findOrFail($id);
        $course->teacher_id = $request->Auth;
        $course->course_title = $request->course_title;
        $course->pre_exp = $request->pre_exp;
        $course->course_date = $request->course_date;
        $course->course_end = $request->course_end;
        $course->cat_id = $request->cat_id;
        $course->course_video_url = $request->course_video_url;
        $course->course_payment_method = $request->course_payment_method;
        $course->course_salary = $request->course_salary;
        $course->certificate_name = $request->certificate_name;
        $course->certificate_branch = $request->certificate_branch;
        $course->certificate_payment = $request->certificate_payment;
        $course->certificate_salary = $request->certificate_salary;
        $course->course_description = $request->course_description;

        if ($request->hasFile('course_image')) {
            $image_name = md5($request->course_image->getClientOriginalName()) . '.' . $request->course_image->getClientOriginalExtension();
            $request->course_image->move(public_path('User/images'), $image_name);
            $course->course_image = $image_name;
        } elseif (!$request->hasFile('course_image')) {
            $course->course_image = 'course_default.jpg';
        }
        if ($request->hasFile('course_video')) {
            $video_name = md5($request->course_video->getClientOriginalName()) . '.' . $request->course_video->getClientOriginalExtension();
            $request->image_video->move(public_path('User/uploads/Videos'), $video_name);
            $course->course_video = $video_name;
        }
        if ($request->has('gender') == 1) {
            $course->gender = $request->gender;
        } elseif ($request->has('gender' == 2)) {
            $course->gender = $request->gender;
        } elseif ($request->has('gender') == 1 && $request->has('gender') == 2) {
            $course->gender = 3;
        }
        $course->save();
        return back()->with('success', 'تم تعديل الدوره بنجاح');


    }
//Get All Courses Depending on Gender and course approved
    public function getAllCourses()
    {
        $settings = Setting::all();

        $courses = Course::where('approved', 1)
            ->where('gender',Auth::user()->gender)->get();
        return view('User.Courses.allCourses', compact('courses', 'courseCategories', 'settings'));
    }
//Get All Categories
    public function getAllCategories()
    {
        $settings = Setting::all();
        $category = DB::table('course_categories')->first();
        $courses = Course::where('cat_id', $category->id)->paginate(6);
        $categories = CourseCategory::all();
        return view('User.Courses.Categories', compact('category', 'courses', 'categories', 'settings'));
    }
//get Single Category
    public function getCategory($id)
    {
        $settings = Setting::all();
        $category = CourseCategory::findOrFail($id);
        $categories = CourseCategory::all();
        $courses = Course::where('cat_id', $category->id)->paginate(6);
        return view('User.Courses.SingleCategory', compact('category', 'categories', 'courses', 'settings'));
    }
//course Perview page to sign up this course
    public function getStartCourse($id)
    {
        $settings = Setting::all();
        $course = Course::findOrFail($id);
        $lectures = Lecture::where('course_id', $course->id)->get();
        $user_id = Auth::id();

        if (!Auth::user()->Courses()->where('course_id', $id)->exists()) {
            $course->users()->attach($user_id);
        }
        return view('User.Courses.SingleCourse', compact('course', 'lectures', 'settings'));
    }

//Start Course Page and its lectures

    public function getSingleCourse($id)
    {
        $settings = Setting::all();
        if (Auth::user()->Courses()->where('course_id', $id)->exists()) {
            $course = Course::findOrFail($id);
            $teacher_id = User::where('id', $course->teacher_id)->first()->id;
            $teacher_cv = CV::where('user_id', $teacher_id)->first()->cv_body;
            $lectures = Lecture::where('course_id', $course->id)->get();
            return view('User.Courses.CoursePreview', compact('course', 'lectures', 'course2', 'teacher_cv', 'settings'));
        } else {
            return back()->with('error', 'يجب اشتراك في الكورس اولاُ');
        }


    }
    // Course Discuss
    public function getCoursediscuss($id)
    {
        $settings = Setting::all();
        $course = Course::findOrFail($id);
        $discusses = Discuss::where('course_id', $course->id)->get();
        return view('User.Courses.Discuss', compact('course', 'discusses', 'settings'));
    }
    //Add Course Discuss by the teacher
    public function postAddDiscuss(Request $request)
    {
        $this->validate($request, ['discuss_body' => 'required']);
        $discuss = new Discuss();
        $discuss->user_id = $request->user_id;
        $discuss->course_id = $request->course_id;
        $discuss->discuss_body = $request->discuss_body;
        $discuss->save();
        return back()->with('success', 'تم اضافه المناقشه');
    }
//delete Course
    public function deleteCourse($id)
    {
        Course::destroy($id);
        return back()->with('success', 'تم مسح الكورس');
    }
//Edit Course Page
    public function getEditCourse($id)
    {
        $settings = Setting::all();
        $course = Course::findOrFail($id);
        if (Auth::id() != $course->teacher_id) {
            return redirect('/');
        }
        $categories = CourseCategory::all();
        $certificate = Certificate::where('course_id', $course->id)->first();
        return view('User.Courses.EditCourse', compact('course', 'categories', 'certificate', 'settings'));
    }
// to dismiss the course from user_course DB
    public function removeCoursePivote($id)
    {
        $course_id = $id;
        $user = Auth::user();
        $user->courses()->detach($course_id);
        return redirect()->back()->with('success', 'تم الانسحاب من الدوره');
    }

}
