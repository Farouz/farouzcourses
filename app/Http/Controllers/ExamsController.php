<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Certificate;
use App\Course;
use App\Exam;
use App\Setting;
use Illuminate\Http\Request;


class ExamsController extends Controller
{
    public function getAddExam($id)
    {
        $settings = Setting::all();

        $course = Course::find($id);
        return view('User.Exam.addExam', compact('course', 'settings'));
    }

    public function postAddExam(Request $request)
    {
        $this->validate($request, ['exam_title' => 'required']);
        $exam = new Exam();
        $exam->exam_title = $request->exam_title;
        $exam->course_id = $request->course_id;
        $exam->save();
        $this->validate($request, ['first_answer' => 'required', 'second_answer' => 'required', 'third_answer' => 'required', 'forth_answer' => 'required']);
        $correct_answer = $request->correct;


        $answer = new Answer();
        $answer->exam_id = $exam->id;
        $answer->answer = $request->first_answer;
        if ($correct_answer == 1) {
            $answer->correct = 1;
        }
        $answer->save();

        $answer = new Answer();
        $answer->exam_id = $exam->id;
        $answer->answer = $request->second_answer;
        if ($correct_answer == 2) {
            $answer->correct = 1;
        }
        $answer->save();

        $answer = new Answer();
        $answer->exam_id = $exam->id;
        $answer->answer = $request->third_answer;
        if ($correct_answer == 3) {
            $answer->correct = 1;
        }
        $answer->save();

        $answer = new Answer();
        $answer->exam_id = $exam->id;
        $answer->answer = $request->forth_answer;
        if ($correct_answer == 4) {
            $answer->correct = 1;
        }
        $answer->save();


        return redirect()->back()->with('success', 'Exam Added');
    }

    public function getTheExam($id)
    {
        $settings = Setting::all();
        $course = Course::findOrFail($id);
        $exams = Exam::where('course_id', $course->id)->get();
        return view('User.Exam.TheExam', compact('exams', 'course', 'settings'));
    }

    public function postFinishExam($id, Request $request)
    {
        $settings = Setting::all();
        $course = Course::findOrFail($id);
        $answers = $request->answers;
        $scores = array();
        foreach ($answers as $answer) {
            $Answer = Answer::findOrFail($answer);

            if ($Answer->correct == 1) {

                array_push($scores, $Answer);
            }

        }
        $scores_count = count($scores); // if>> 3 correct
        $allAnswers = Answer::count(); // 12 all
        $halfAnswers = $allAnswers / 2; // 6 half of count
        $correct_count = $halfAnswers / 2; // 3
        if ($correct_count == $scores_count) {
            $certificate = new Certificate();
            $certificate->user_id = Auth::id();
            $certificate->course_id = $course->id;
            $certificate->certificate_name = $course->certificate_name;
            $certificate->certificate_branch = $course->certificate_branch;
            $certificate->certificate_payment = $course->certificate_payment;
            $certificate->certificate_salary = $course->certificate_salary;
            $certificate->save();
            return view('User.Certificate.Certificate', compact('course', 'settings'))->with('success', 'Congratulation');
        } else {
            return back()->with('error', 'sorry you failed in the exam , please try again later');
        }
    }
}
