<?php

namespace App\Http\Controllers;

use App\Country;
use App\Course;
use App\CourseCategory;
use App\CV;
use App\Discuss;
use App\Interest;
use App\CourseUser;
use App\Lecture;
use App\Note;
use App\Talk;
use App\User;
use Carbon\Carbon;
use Dotenv\Validator;
use Illuminate\Http\Request;
use App\Notifications\VerfiyEmail;
use Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Setting;


class UsersController extends Controller
{
    public function index()
    {
        $customers = Talk::all();
        $settings = Setting::all();
        $teachers = User::where('is_teacher', 1)->get();
        $cats = CourseCategory::all();
        $courses = Course::where('approved', 1)->get();
        return view('User.home', compact('cats', 'courses', 'teachers', 'settings', 'customers'));
    }

    public function getSignUp()
    {
        $settings = Setting::all();

        $countries = Country::all();
        return view('User.signUp', compact('countries', 'settings'));
    }

    public function postSignUp(Request $request)
    {
        $this->validate($request, ['fullName' => 'required|alpha_dash', 'userName' => 'required|alpha_dash|unique:users,userName', 'email' => 'required|email|unique:users,email', 'phone' => 'required|numeric|min:11', 'password' => 'required|min:5|max:15|confirmed', 'password_confirmation' => 'required', 'country' => 'required', 'gender' => 'required', 'is_teacher' => 'required_if:is_trainee,null', 'terms' => 'required|accepted',]);
        $user = new User();
        $user->fullName = $request->fullName;
        $user->userName = $request->userName;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->country = $request->country;
        $user->gender = $request->gender;
        $user->Image = 'default.png';
        $user->qualification = 'None';
        $user->spcialization = 'None';
        $user->job_title = 'None';
        if ($request->has('is_teacher')) {
            $user->is_teacher = 1;
        } elseif ($request->has('is_trainee')) {
            $user->is_teacher = 0;
        } elseif ($request->has('is_teacher') && $request->has('is_trainee')) {
            $user->is_teacher = 2;
        }
        if ($request->has('terms')) {
            $user->terms = 1;
        } else {
            $user->terms = 0;
        }
        $user->token = str_random(25);
        $user->sendVerificationEmail();
        $user->save();
        if ($user->active == 1 && $user->approved == 1) {

            return redirect()->route('home')->with('success', 'welcome');
        } else {
            return back()->with('success', 'Please Check Your Mail To active your Account');
        }


    }

    public function postSigIn(Request $request)
    {

        if (!auth()->attempt(\request(['userName', 'password']))) {
            return back()->withErrors(['message' => 'Email Or Password Not Correct']);
        };
        return redirect()->route('home')->with('success', 'Welcome ');

    }

    public function logout()
    {
        auth()->logout();
        return back();
    }

    public function getUserProfile($id)
    {
        $discuss = Discuss::all();
        $notes = Note::all();
        $settings = Setting::all();
        $categories = CourseCategory::all();
        $interests = CourseCategory::all();
        $countries = Country::all();
        $user = User::findOrFail($id);
        $teacherCourses = Course::where('teacher_id', $user->id)->get();
        if (!$user->interests == null) {
            $user_interests = json_decode($user->interests);
        } else {
            $user_interests = null;
        }
        return view('User.Profiles.Profile', compact('user', 'countries', 'teacherCourses', 'interests', 'categories', 'myCourses', 'user_interests', 'CurrentCourses', 'settings', 'discuss', 'notes'));
    }

    public function postEditProfile($id, Request $request)
    {
        $this->validate($request, ['fullName' => 'regex:/^([^0-9]*)$/|string', 'username' => 'regex:/^([^0-9]*)$/|unique:users,userName', 'email' => 'email', 'phone' => 'numeric', 'qualification' => 'regex:/^([^0-9]*)$/', 'spcialization' => 'regex:/^([^0-9]*)$/', 'Image' => 'mimes:jpeg,jpg,png']);
        $user = User::findOrFail($id);
        $user->fullName = $request->fullName;
        $user->userName = $request->userName;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->country = $request->country;
        $user->gender = $request->gender;
        $user->is_teacher = $request->is_teacher;
        $user->qualification = $request->qualification;
        $user->spcialization = $request->spcialization;
        if ($request->hasFile('Image')) {
            $image_name = md5($request->Image->getClientOriginalName()) . '.' . $request->Image->getClientOriginalExtension();
            $request->Image->move(public_path('User/images'), $image_name);
            $user->Image = $image_name;

        }
        $user->save();
        return back()->with('success', 'Profile Updated');

    }

    public function getAbout()
    {
        $settings = Setting::all();

        return view('User.About.about', compact('settings'));
    }

    public function getPolicity()
    {
        $settings = Setting::all();
        return view('User.Policity.Policity', compact('settings'));
    }

    public function postRestPassword($id, Request $request)
    {
        $user = User::FindOrFail($id);
        $oldpass = $user->password;
        $this->validate($request, ['old_password' => 'required', 'password' => 'required|min:5|max:15|confirmed']);
        if (($request->old_password) == $oldpass) {
            $user->password = $request->password;
            $user->save();
            return back()->with('success', 'تم تعديل الرقم السري');
        } else {
            return back()->with('error', 'الرقم السري القديم خطأ');
        }
    }

    public function postAddCv(Request $request)
    {
        $this->validate($request, ['cv_url' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', 'cv_body' => 'required_if:cv_url,null']);
        $cv = new CV();
        $cv->user_id = $request->user_id;
        $cv->cv_url = $request->cv_url;
        $cv->cv_body = $request->cv_body;
        $cv->save();
        return back()->with('success', 'تم اضافه السيره الذاتيه بنجاح');
    }

    public function postAddInterest(Request $request)
    {
        $user = Auth::user();
        $interests = $request->interests;
        $interests_json = json_encode($interests);

        $user->interests = $interests_json;
        if ($request->has('interest_type') == 1) {
            $user->interest_type = 1;
        } elseif ($request->has('interest_type') == 2) {
            $user->interest_type = 2;
        } elseif ($request->has('interest_type') == 1 && $request->has('interest_type') == 2) {
            $user->interest_type = 3;
        }
        $user->save();
        return back()->with('success', 'تم اضافه الاهتمام');
    }

    public function DeleteInterest($id)
    {
        $interest = $id;

        $user = Auth::user();
        $interests = json_decode($user->interests);
        foreach ($interests as $key => $value) {
            if ($value == $interest) {
                unset($interests[$key]);
            }
        }
        return back()->with('success', 'تم الغاء الاهتمام');
    }

    public function api()
    {
        $users = User::where('id', '5')->first();
        if (!$users) {
            $msg = 'there is no user with id 1';
            return response()->json(['key' => 'fail', 'value' => '0', 'msg' => $msg]);
        }
        $user_data['fullName'] = $users->fullName;
        $user_data['email'] = $users->email;
        $user_data['phone'] = '0' . $users->phone;
        $user_data['active'] = $users->active;
        return response()->json(['key' => 'success', 'value' => '1', 'data' => $user_data]);


    }

    public function changePassword(Request $request)
    {
        $this->validate($request, ['email' => 'required|email', 'password' => 'required|confirmed', 'password_confirmation' => 'required']);

        $user = User::where('email', $request['email'])->first();
        if (!$user) {
            return back()->with('error', 'email incorrect');
        }
        $user->password = bcrypt($request['password']);
        $user->update();
        Auth::login($user);
        return redirect()->route('home')->with('success', 'password changed and Welcome ');
    }

}
