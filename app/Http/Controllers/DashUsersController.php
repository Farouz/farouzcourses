<?php

namespace App\Http\Controllers;

use App\Country;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashUsersController extends Controller
{
    public function getAllUsers()
    {
        $users = User::all();
        return view('Dashboard.Users.AllUsers', compact('users'));
    }

    public function getDeleteUser($id)
    {
        User::destroy($id);
        return back()->with('success', 'تم مسح العضو');
    }

    public function getApproveUser($id)
    {
        User::where('id', $id)->update(['approved' => '1']);

        $user = User::findOrFail($id);
        Auth::login($user);

        return back()->with('success', 'تم تفعيل العضو');
    }

    public function getAddUser()
    {
        $countries = Country::all();
        return view('Dashboard.Users.AddUser', compact('countries'));
    }

    public function postAddUser(Request $request)
    {
        $this->validate($request, ['fullName' => 'required|alpha_dash', 'userName' => 'required|alpha_dash|unique:users,userName', 'qualification' => 'required', 'job_title' => 'required', 'spcialization' => 'required', 'email' => 'required|email|unique:users,email', 'phone' => 'required|min:11|numeric', 'password' => 'required|min:5|max:15', 'country' => 'required', 'gender' => 'required', 'is_teacher' => 'required_if:is_trainee,null', 'image' => 'required|mimes:jpeg,jpg,png']);
        $user = new User();
        $user->fullName = $request->fullName;
        $user->userName = $request->userName;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->qualification = $request->qualification;
        $user->spcialization = $request->spcialization;
        $user->job_title = $request->job_title;
        $user->country = $request->country;
        $user->gender = $request->gender;
        $user->approved = 1;
        $user->active = 1;
        if ($request->hasFile('image')) {
            $image_name = md5($request->image->getClientOriginalName()) . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('User/images'), $image_name);
            $user->Image = $image_name;
        } else {
            $user->Image = 'default.jpg';
        }
        if ($request->has('is_teacher')) {
            $user->is_teacher = 1;
        } elseif ($request->has('is_trainee')) {
            $user->is_teacher = 0;
        } elseif ($request->has('is_teacher') && $request->has('is_trainee')) {
            $user->is_teacher = 2;
        }
        if ($request->has('terms')) {
            $user->terms = 1;
        } else {
            $user->terms = 0;
        }
        $user->save();
        return redirect()->route('GET_ALL_USERS')->with('success', 'User Added');

    }

    public function getEditUser($id)
    {
        $countries = Country::all();
        $user = User::find($id);
        return view('Dashboard.Users.EditUser', compact('user', 'countries'));
    }

    public function postEditUser($id, Request $request)
    {
        $this->validate($request, ['fullName' => 'required|regex:/^([^0-9]*)$/', 'userName' => 'required|regex:/^([^0-9]*)$/', 'qualification' => 'required', 'job_title' => 'required', 'spcialization' => 'required', 'email' => 'required|email', 'phone' => 'required|min:11|numeric', 'password' => 'required', 'country' => 'required', 'gender' => 'required', 'is_teacher' => 'required_if:is_trainee,null', 'image' => 'mimes:jpeg,jpg,png']);

        $user = User::find($id);
        if ($request->has('is_teacher')) {
            $user->is_teacher = 1;
        } elseif ($request->has('is_trainee')) {
            $user->is_teacher = 0;
        } elseif ($request->has('is_teacher') && $request->has('is_trainee')) {
            $user->is_teacher = 2;
        }
        if ($request->hasFile('image')) {
            $image_name = md5($request->image->getClientOriginalName()) . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('User/images'), $image_name);
        } else {
            $image_name = $user->Image;
        }
        User::where('id', $id)->update(['fullName' => $request->fullName, 'userName' => $request->userName, 'email' => $request->email, 'phone' => $request->phone, 'password' => bcrypt($request->password), 'qualification' => $request->qualification, 'job_title' => $request->job_title, 'country' => $request->country, 'gender' => $request->gender, 'Image' => $image_name, 'terms' => 1,

        ]);

        $user->save();
        return redirect()->back()->with('success', 'Profile Updated');
    }

    public function getUserProfile($id)
    {
        $user = User::findOrFail($id);
        return view('Dashboard.Users.Profile', compact('user'));
    }

}
