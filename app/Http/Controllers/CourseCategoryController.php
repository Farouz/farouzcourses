<?php

namespace App\Http\Controllers;

use App\CourseCategory;
use Illuminate\Http\Request;

class CourseCategoryController extends Controller
{
    //Add New Category from Dashboard
    public function getAddCategory()
    {
        return view('Dashboard.CourseCategory.addCategory');
    }

    // the post function to add category
    public function postAddCategory(Request $request)
    {
//       validation
        $this->validate($request, ['title_en' => 'required', 'title_ar' => 'required',]);
        $category = new CourseCategory();
        $category->title_en = $request->title_en;
        $category->title_ar = $request->title_ar;
        $category->save();
        return redirect()->route('GET_ALL_CATEGORIES')->with('success', 'Category Add ');
    }

//To Get All Categories
    public function getAllCategory()
    {
        $categories = CourseCategory::all();
        return view('Dashboard.CourseCategory.AllCategories', compact('categories'));
    }
    //CATEGORY CRUD
    //Delete Category
    public function getDeleteCategory($id)
    {
        CourseCategory::destroy($id);
        return redirect()->back()->with('alert', 'Course Category Deleted');
    }

//Edit Category Form
    public function getEditCategory($id)
    {
        $category = CourseCategory::find($id);
        return view('Dashboard.CourseCategory.EditCategory', compact('category'));
    }

    //The post Edit
    public function postEditCategory($id, Request $request)
    {
        $this->validate($request, ['title_en' => 'required', 'title_ar' => 'required',]);
        $cat = CourseCategory::findOrFail($id);
        $cat->title_en = $request->title_en;
        $cat->title_ar = $request->title_ar;
        $cat->update();
        return redirect()->route('GET_ALL_CATEGORIES')->with('success', 'Category Updated');
    }
}
