<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VerifyController extends Controller
{
    public function verify($token)
    {
        $user = \App\User::where('token', $token)->first();
        User::where('token', $token)->update(['token' => null, 'active' => 1]);
        if ($user->approved == 1){
            Auth::login($user);
        }
        return redirect()->route('home')->with('success', 'please wait for a proved');
    }
}
