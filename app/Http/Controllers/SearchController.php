<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseCategory;
use App\User;
use Illuminate\Http\Request;
use App\Setting;

class SearchController extends Controller
{
    public function postSearchCourse(Request $request)
    {
        $settings = Setting::all();
        $courses = Course::
        where('course_title', 'LIKE', '%' . $request->course_search . '%')->OrWhere('teacher_id', '=', $request->teacher_id)->OrWhere('cat_id', '=', $request->cat_id)->get();
        return view('User.Search.CourseSearch', compact('courses', 'settings'));
    }
}
