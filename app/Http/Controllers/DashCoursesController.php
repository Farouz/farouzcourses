<?php

namespace App\Http\Controllers;

use App\Course;
use App\Certificate;
use App\CourseCategory;
use App\User;
use Illuminate\Http\Request;

class DashCoursesController extends Controller
{
    public function getAllCourses()
    {
        $courses = Course::all();
        return view('Dashboard.Courses.AllCourses', compact('courses'));
    }

    public function DeleteCourse($id)
    {
        Course::destroy($id);
        return back()->with('success', 'تم مسح الدوره ');
    }

    public function getApproveCourse($id)
    {
        Course::where('id', $id)->update(['approved' => '1']);
        return back()->with('success', 'تم تفعيل الدوره');
    }

    public function getAddCourse()
    {
        $categories = CourseCategory::all();
        $teachers = User::where('is_teacher', 1)->get();
        return view('Dashboard.Courses.AddCourse', compact('teachers', 'categories'));
    }

    public function postAddCourse(Request $request)
    {
        $this->validate($request, ['course_title' => 'required|regex:/^([^0-9]*)$/|string', 'pre_exp' => 'regex:/^([^0-9]*)$/|string', 'cat_id' => 'required', 'course_video' => 'mimes:mp4,webm,flv,mkv,avi | max:20000', 'course_image' => 'image|max:20000', 'course_video_url' => 'required_if:course_video,null', 'course_description' => 'required|regex:/^([^0-9]*)$/|string', 'gender' => 'required', 'teacher_id' => 'required', 'course_payment_method' => 'required', 'course_salary' => 'min:0|required_if:course_payment_method,2', 'course_date' => 'required|date', 'course_end' => 'required|date|after:course_date', 'certificate_name' => 'nullable|string', 'certificate_branch' => 'nullable|string', 'certificate_salary' => 'min:0|required_if:certificate_payment,2']);
        $course = new Course();
        $course->course_title = $request->course_title;
        $course->pre_exp = $request->pre_exp;
        $course->course_date = $request->course_date;
        $course->course_end = $request->course_end;
        $course->cat_id = $request->cat_id;
        $course->course_video_url = $request->course_video_url;
        $course->course_description = $request->course_description;
        $course->course_payment_method = $request->course_payment_method;
        $course->course_salary = $request->course_salary;
        $course->teacher_id = $request->teacher_id;
        $course->certificate_name = $request->certificate_name;
        $course->certificate_branch = $request->certificate_branch;
        $course->certificate_payment = $request->certificate_payment;
        $course->certificate_salary = $request->certificate_salary;
        $course->approved = 1;
        if ($request->hasFile('course_image')) {
            $image_name = md5($request->course_image->getClientOriginalName()) . '.' . $request->course_image->getClientOriginalExtension();
            $request->course_image->move(public_path('User/images'), $image_name);
            $course->course_image = $image_name;
        } elseif (!$request->hasFile('course_image')) {
            $course->course_image = 'course_default.jpg';
        }
        if ($request->hasFile('course_video')) {
            $video_name = md5($request->course_video->getClientOriginalName()) . '.' . $request->course_video->getClientOriginalExtension();
            $request->image_video->move(public_path('User/uploads/Videos'), $video_name);
            $course->course_video = $video_name;
        }
        if ($request->has('gender') == 1) {
            $course->gender = $request->gender;
        } elseif ($request->has('gender' == 2)) {
            $course->gender = $request->gender;
        } elseif ($request->has('gender') == 1 && $request->has('gender') == 2) {
            $course->gender = 3;
        }

        $course->save();
        return redirect()->route('GET_ALL_COURSES_DASH')->with('success', 'Course Added Successfully');

    }

    public function postEditCourse($id, Request $request)
    {
        $this->validate($request, ['course_title' => 'required|regex:/^([^0-9]*)$/|string', 'pre_exp' => 'regex:/^([^0-9]*)$/|string', 'cat_id' => 'required', 'course_video' => 'mimes:mp4,webm,flv,mkv,avi | max:20000', 'course_image' => 'image|max:20000', 'course_video_url' => 'required_if:course_video,null', 'course_description' => 'required|regex:/^([^0-9]*)$/|string', 'gender' => 'required', 'course_payment_method' => 'required', 'course_salary' => 'min:0|required_if:course_payment_method,2', 'course_date' => 'required|date', 'course_end' => 'required|date|after:course_date', 'teacher_id' => 'required', 'certificate_name' => 'nullable|string', 'certificate_branch' => 'nullable|string', 'certificate_salary' => 'min:0|required_if:certificate_payment,2']);

        $course = Course::findOrFail($id);
        if ($request->hasFile('course_image')) {
            $image_name = md5($request->course_image->getClientOriginalName()) . '.' . $request->course_image->getClientOriginalExtension();
            $request->course_image->move(public_path('User/images'), $image_name);

        } else {
            $image_name = $course->course_image;
        }
        if ($request->hasFile('course_video')) {
            $video_name = md5($request->course_video->getClientOriginalName()) . '.' . $request->course_video->getClientOriginalExtension();
            $request->image_video->move(public_path('User/uploads/Videos'), $video_name);
            $course->course_video = $video_name;
        }
        if ($request->has('gender') == 1) {
            $course->gender = $request->gender;
        } elseif ($request->has('gender' == 2)) {
            $course->gender = $request->gender;
        } elseif ($request->has('gender') == 1 && $request->has('gender') == 2) {
            $course->gender = 3;
        }
        Course::where('id', $id)->update(['course_title' => $request->course_title, 'pre_exp' => $request->pre_exp, 'teacher_id' => $request->teacher_id, 'course_description' => $request->course_description, 'course_date' => $request->course_date, 'course_end' => $request->course_end, 'cat_id' => $request->cat_id, 'course_video_url' => $request->course_video_url, 'course_payment_method' => $request->course_payment_method, 'course_salary' => $request->course_salary, 'certificate_name' => $request->certificate_name, 'certificate_branch' => $request->certificate_branch, 'certificate_payment' => $request->certificate_payment, 'certificate_salary' => $request->certificate_salary, 'course_image' => $image_name]);
        $course->save();
        return redirect()->route('GET_SHOW_COURSE_DASH', $course->id)->with('success', 'course updated');
    }

    public function getShowCourse($id)
    {
        $course = Course::findOrFail($id);
        return view('Dashboard.Courses.CourseProfile', compact('course'));
    }

    public function getEditCourse($id)
    {
        $categories = CourseCategory::all();
        $teachers = User::where('is_teacher', 1)->get();
        $course = Course::findOrFail($id);
        return view('Dashboard.Courses.EditCourse', compact('course', 'categories', 'teachers'));
    }


}
