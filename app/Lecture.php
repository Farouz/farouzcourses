<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    protected $fillable = ['lecture_title', 'lecture_description', 'lecture_video', 'lecture_url', 'lecture_file', 'teacher_id', 'course_id'];

    public function Users()
    {
        return $this->belongsToMany(User::class, 'lectures_users', 'lecture_id', 'user_id');
    }

    public function Course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
