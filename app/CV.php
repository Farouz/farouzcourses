<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CV extends Model
{
    protected $fillable = ['user_id', 'cv_url', 'cv_body'];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    protected $table = 'cvs';
}
