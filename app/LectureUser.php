<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LectureUser extends Model
{
    protected $table='lectures_users';
}
