<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discuss extends Model
{
    protected $fillable = ['discuss_body', 'user_id', 'course_id'];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function Course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
