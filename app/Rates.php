<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rates extends Model
{
    protected $table='users_rates';
    protected $fillable=[
      'user_id','course_id','rate'
    ];
}
